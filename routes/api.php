<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
    'prefix' => 'provider'
], function ($router) {
    Route::get('providers', 'ProviderController@index');
    Route::post('create', 'ProviderController@prepareCreateOrUpdateData');
    Route::patch('{id}', 'ProviderController@prepareCreateOrUpdateData');
    Route::get('{id}', 'ProviderController@get');
    Route::delete('{id}', 'ProviderController@delete');
});

Route::group([
    'prefix' => 'seller'
], function ($router) {
    Route::get('sellers', 'SellerController@index');
    Route::post('create', 'SellerController@prepareCreateOrUpdateData');
    Route::patch('{id}', 'SellerController@prepareCreateOrUpdateData');
        Route::get('{id}', 'SellerController@get');
    Route::delete('{id}', 'SellerController@delete');
});

Route::group([
    'prefix' => 'product'
], function ($router) {
    Route::get('products', 'ProductController@index');
    Route::post('create', 'ProductController@prepareCreateOrUpdateData');
    Route::patch('{id}', 'ProductController@prepareCreateOrUpdateData');
    Route::get('{id}', 'ProductController@get');
    Route::delete('{id}', 'ProductController@delete');
});

Route::group([
    'prefix' => 'sell'

], function ($router) {
    Route::get('sells', 'SellController@index');
    Route::post('create', 'SellController@prepareCreateOrUpdateData');
});

Route::group([
    'prefix' => 'person'

], function ($router) {
    Route::get('persons', 'PersonController@index');
    Route::post('create', 'PersonController@prepareCreateOrUpdateData');
});