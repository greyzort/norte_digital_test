<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sell;
use App\Models\Product;
use App\Models\ProductSell;
use App\Constants\CHttpStatus;
use App\Http\Requests\SellRequest;

class SellController extends Controller
{
         /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sells = Sell::all();

        $data = [
            'code' => CHttpStatus::OK,
            'data' => [
                'sells' => $sells
            ]
        ];
        
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function prepareCreateOrUpdateData(SellRequest $request, $provider_id, $sell_id = null)
    {
        if($sell_id) {
            if ( !$sell=Sell::find($sell_id) ) {
                return response()->json([
                    'code' => CHttpStatus::NOT_FOUND,
                    'message' => trans('sell.not_found')
                ]);            
            }   
        }

        foreach($request->products as $product) {
            if( $validator_result = $this->validateData( $product, ProductSell::rules(), trans('validation') )) {
                return $validator_result;  
            }
        }

        $total_price = 0;

        $sell_data = [
            'sell_date' => $request->sell_date,
            'seller_id' => $request->seller_id,
            'client_id' => $request->client_id,
            'total_price' => $total_price
        ];

        if(!$sell_id){
            $sell = $this->create($sell_data);
        } else {
            // $sell = $this->update($sell, $sell);
        }
        foreach($request->products as $product) {
            $db_product = Product::find($product['product_id']);
            $product_sell = ProductSell::createProductSell($db_product, $product['quantity'], $sell->id);
            $total_price = $total_price + $product_sell->subtotal;
        }

        $sell->total_price = $total_price;
        $sell->save();

        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'sell' => $sell
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create($sell_data)
    {
        return  Sell::create($sell_data);        
    }
}
