<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Person;
use App\Constants\CHttpStatus;
use App\Http\Requests\PersonRequest;
use Illuminate\Http\JsonResponse;
use App\Models\Address;

class PersonController extends Controller
{
         /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $persons = Person::all();

        $data = [
            'code' => CHttpStatus::OK,
            'data' => [
                'persons' => $persons
            ]
        ];
        
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function prepareCreateOrUpdateData(PersonRequest $request, $provider_id, $person_id = null)
    {
        $person = Person::createOrUpdatePerson($request, null);

        if( get_class($person) == JsonResponse::class){
            return $person;
        }else{
            return response()->json([
                'code' => CHttpStatus::OK,
                'data' => [
                    'person' => $person
                ]
            ]);
        }   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create($person_data)
    {
        return  Person::create($person_data);        
    }
}
