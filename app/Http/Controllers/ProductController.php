<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Constants\CHttpStatus;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(10);

        $data = [
            'code' => CHttpStatus::OK,
            'data' => [
                'products' => $products
            ]
        ];
        
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function prepareCreateOrUpdateData(ProductRequest $request, $provider_id, $product_id = null)
    {
        if($product_id) {
            if ( !$product=Product::find($product_id) ) {
                return response()->json([
                    'code' => CHttpStatus::NOT_FOUND,
                    'message' => trans('product.not_found')
                ]);            
            }   
        }

        $product_data = [
            'name' => $request->name,
            'price' => $request->price,
            'stock' => $request->stock,
            'provider_id' => $request->provider_id
        ];

        if(!$product_id){
            $product = $this->create($product_data);
        } else {
            $product = $this->update($product, $product);
        }

        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'product' => $product
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create($product_data)
    {
        return  Product::create($product_data);        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentPlatform  $paymentPlatform
     * @return \Illuminate\Http\Response
     */
    public function get($product_id)
    {
        if ( !$product=Product::find($product_id) ) {
            return response()->json([
                'code' => CHttpStatus::NOT_FOUND,
                'message' => trans('product.not_found')
            ]);            
        }
        
        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'product' => $product
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentPlatform  $paymentPlatform
     * @return \Illuminate\Http\Response
     */
    public function update($product, $product_data)
    {
        $product->fill($product_data)->save();
        return $product;
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentPlatform  $paymentPlatform
     * @return \Illuminate\Http\Response
     */
    public function delete($product_id)
    {
        if ( !$product=Product::find($product_id) ) {
            return response()->json([
                'code' => CHttpStatus::NOT_FOUND,
                'message' => trans('product.not_found')
            ]);            
        }
        
        $product->delete();

        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'message' => trans('product.deleted')
            ]
        ]);
    }
}
