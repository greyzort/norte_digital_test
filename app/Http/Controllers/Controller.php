<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Validator;
use App\Constants\CHttpStatus;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function validateData ( $data, $rules, $message ) {

        $validator = Validator::make($data, $rules, $message);
        if( !is_null($validator) && ($validator->fails()) ) {
            $data = [
                'message' => 'error',
                'data' => [
                  'errors' => $validator->messages()
                ]
              ];
              return response()->json($data, CHttpStatus::INTERNAL_SERVER_ERROR);
        }

        return null;
        
    }

}
