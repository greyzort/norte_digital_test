<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Provider;
use App\Models\Address;
use App\Constants\CHttpStatus;
use App\Http\Requests\ProviderRequest;

class ProviderController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = Provider::paginate(10);

        $data = [
            'code' => CHttpStatus::OK,
            'data' => [
                'providers' => $providers
            ]
        ];
        
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function prepareCreateOrUpdateData(ProviderRequest $request, $provider_id = null)
    {
        if($provider_id) {
            if ( !$provider=Provider::find($provider_id) ) {
                return response()->json([
                    'code' => CHttpStatus::NOT_FOUND,
                    'message' => trans('provider.not_found')
                ]);            
            }   
        }
        
        $address = Address::createOrUpdateAddress($request, $provider_id ? $provider->address_id : null);

        $provider_data = [
            'name' => $request->name,
            'rut' => $request->rut,
            'phone' => $request->phone,
            'web_site' => $request->web_site,
            'address_id' => $address->id
        ];

        if(!$provider_id){
            $provider = $this->create($provider_data);
        } else {
            $provider = $this->update($provider, $provider_data);
        }

        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'provider' => $provider
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create($provider_data)
    {
        return  Provider::create($provider_data);        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentPlatform  $paymentPlatform
     * @return \Illuminate\Http\Response
     */
    public function get($provider_id)
    {
        if ( !$provider=Provider::find($provider_id) ) {
            return response()->json([
                'code' => CHttpStatus::NOT_FOUND,
                'message' => trans('provider.not_found')
            ]);            
        }
        
        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'provider' => $provider
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentPlatform  $paymentPlatform
     * @return \Illuminate\Http\Response
     */
    public function update($provider, $provider_data)
    {
        $provider->fill($provider_data)->save();
        return $provider;
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentPlatform  $paymentPlatform
     * @return \Illuminate\Http\Response
     */
    public function delete($provider_id)
    {
        if ( !$provider=Provider::find($provider_id) ) {
            return response()->json([
                'code' => CHttpStatus::NOT_FOUND,
                'message' => trans('provider.not_found')
            ]);            
        }
        
        $provider->delete();

        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'message' => trans('provider.deleted')
            ]
        ]);
    }
}
