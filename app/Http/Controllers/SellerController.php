<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Seller;
use App\Models\Person;
use App\Constants\CHttpStatus;
use App\Http\Requests\SellerRequest;
use App\Models\Address;
use Illuminate\Http\JsonResponse;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sellers = Seller::paginate(10);

        $data = [
            'code' => CHttpStatus::OK,
            'data' => [
                'sellers' => $sellers
            ]
        ];
        
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function prepareCreateOrUpdateData(SellerRequest $request, $seller_id = null)
    {
        if($seller_id) {
            if ( !$seller=Seller::find($seller_id) ) {
                return response()->json([
                    'code' => CHttpStatus::NOT_FOUND,
                    'message' => trans('seller.not_found')
                ]);            
            }   
        }
        $person = Person::createOrUpdatePerson($request, $seller_id ? $seller->person_id : null);

        if( get_class($person) == JsonResponse::class){
            return $person;
        }

        $seller_data = [
            'person_id' => $person->id,
            'born_date' => $request->born_date,
            'email' => $request->email
        ];

        if(!$seller_id){
            $seller = $this->create($seller_data);
        } else {
            $seller = $this->update($seller, $seller_data);
        }

        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'seller' => $seller
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create($seller_data)
    {
        return  Seller::create($seller_data);        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentPlatform  $paymentPlatform
     * @return \Illuminate\Http\Response
     */
    public function get($seller_id)
    {
        if ( !$seller=Seller::find($seller_id) ) {
            return response()->json([
                'code' => CHttpStatus::NOT_FOUND,
                'message' => trans('seller.not_found')
            ]);            
        }
        
        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'seller' => $seller
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentPlatform  $paymentPlatform
     * @return \Illuminate\Http\Response
     */
    public function update($seller, $seller_data)
    {
        $seller->fill($seller_data)->save();
        return $seller;
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentPlatform  $paymentPlatform
     * @return \Illuminate\Http\Response
     */
    public function delete($seller_id)
    {
        if ( !$seller=Seller::find($seller_id) ) {
            return response()->json([
                'code' => CHttpStatus::NOT_FOUND,
                'message' => trans('seller.not_found')
            ]);            
        }
        
        $seller->delete();

        return response()->json([
            'code' => CHttpStatus::OK,
            'data' => [
                'message' => trans('seller.deleted')
            ]
        ]);
    }
}
