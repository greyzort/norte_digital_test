<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SellRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sell_date' => 'required|date',
            'seller_id' => 'required',
            'client_id' => 'required',
            'products'  => 'required|array',
            'products.*'  => 'required|array',
            'products.*.product_id' => 'required'
        ];
    }
}
