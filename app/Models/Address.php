<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Constants\CHttpStatus;

class Address extends Model
{
    protected $fillable = [
        'street',
        'number',
        'comunity',
        'city'
    ];

    public static function createOrUpdateAddress($data, $address_id) {

        if($address_id) {
            if ( !$address=Address::find($address_id) ) {
                return response()->json([
                    'code' => CHttpStatus::NOT_FOUND,
                    'message' => trans('adrress.not_found')
                ]);            
            }   
        }

        $address_data = [
            'street' => $data->street,
            'number' => $data->number,
            'comunity' => $data->comunity,
            'city' => $data->city
        ];
        if($address_id){
            $address->fill($address_data)->save();
        }else {
            $address = Address::Create($address_data);
        }
        return $address;
    }
}
