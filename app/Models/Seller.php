<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Person;

class Seller extends Model
{
    protected $fillable = [
        'person_id',
        'born_date',
        'email'
    ];

    public function person()
    {
        return $this->belongsTo(Person::class);
    }
}
