<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Provider;

class Product extends Model
{
    protected $fillable = [
        'name',
        'price',
        'stock',
        'provider_id'
    ];

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }
}
