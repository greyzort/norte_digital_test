<?php

namespace App\Models;
use App\Constants\CHttpStatus;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\Model;
use App\Models\Address;

class Person extends Model
{
    protected $table = 'persons';

    protected $fillable = [
        'rut',
        'name',
        'last_name',
        'phone',
        'address_id'
    ];

    public static function createOrUpdatePerson($request, $person_id) {

        if($person_id) {
            if ( !$person=Person::find($person_id) ) {
                return response()->json([
                    'code' => CHttpStatus::NOT_FOUND,
                    'message' => trans('person.not_found')
                ]);            
            }   
        }

        $address = Address::createOrUpdateAddress($request, $person_id ? $person->address_id : null);

        if( get_class($address) == JsonResponse::class){
            return $address;
        }
        $person_data = [
            'rut' => $request->rut,
            'name' => $request->name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'address_id' => $address->id
        ];

        if($person_id){
            $person->fill($person_data)->save();
        }else {
            $person = Person::create($person_data);
        }
        return $person;
    }
}
