<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Provider extends Model
{
    protected $fillable = [
        'name',
        'rut',
        'phone',
        'web_site',
        'address_id'
    ];

    public function product()
    {
        return $this->hasMany(Product::class);
    }
}
