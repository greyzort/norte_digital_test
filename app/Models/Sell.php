<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    protected $fillable = [
        'sell_date',
        'seller_id',
        'client_id',
        'total_price'
    ];
}
