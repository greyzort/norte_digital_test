<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSell extends Model
{
    protected $fillable = [
        'quantity',
        'product_id',
        'sell_id',
        'unit_price',
        'subtotal'
    ];

    public static function rules () {
    
        return [
            'quantity' => 'required|integer',
            'product_id' => 'required',
        ];
    }

    public static function createProductSell($product, $quantity, $sell_id) {
        $produc_sell_data = [
            'quantity' => $quantity,
            'product_id' => $product->id,
            'sell_id' => $sell_id,
            'unit_price' => $product->price,
            'subtotal' => $quantity * $product->price
        ];

        return ProductSell::create($produc_sell_data);
    }
}
