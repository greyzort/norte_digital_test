<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use App\Models\Provider;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->word .' ' . $faker->word,
        'price' => $faker->randomFloat,
        'stock' => $faker->randomNumber,
        // 'provider_id' => function () {
        //     return factory(Provider::class)->create()->id;
        // },
    ];
});
