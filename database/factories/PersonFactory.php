<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Person;
use App\Models\Address;
use Faker\Generator as Faker;

$factory->define(Person::class, function (Faker $faker) {
    return [
        'rut'       => $faker->randomNumber,
        'name'      => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone'     => $faker->phoneNumber,
        'address_id'=> function () {
            return factory(Address::class)->create()->id;
        }
    ];
});
