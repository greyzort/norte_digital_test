<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Provider;
use App\Models\Address;
use Faker\Generator as Faker;

$factory->define(Provider::class, function (Faker $faker) {
    return [
        'rut'       => $faker->randomNumber,
        'name'      => $faker->name,
        'phone'     => $faker->phoneNumber,
        'web_site'  => $faker->url,
        'address_id'=> function () {
            return factory(Address::class)->create()->id;
        }
    ];
});
