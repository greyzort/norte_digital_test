<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Seller;
use App\Models\Person;
use Faker\Generator as Faker;

$factory->define(Seller::class, function (Faker $faker) {
    return [
        'person_id' => function () {
            return factory(Person::class)->create()->id;
        },
        'born_date' => $faker->date,
        'email' => $faker->unique()->safeEmail
    ];
});
