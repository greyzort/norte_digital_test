<?php

use Illuminate\Database\Seeder;
use App\Models\Provider;
use App\Models\Product;

class ProviderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Provider::class, 100)->create()->each(function ($provider) {
            $provider->product()->createMany(factory(Product::class,5)->make()->toArray());
        });
    }
}
