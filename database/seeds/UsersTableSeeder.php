<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    private function getData() {
        return $data = [
            [
                'name' => 'root',
                'email' => 'admin@admin.com',
                'password' => bcrypt('123456'),
            ]
        ];
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getData() as $data) {
            User::create($data);
        }
    }
}
