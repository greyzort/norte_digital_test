<?php

use Illuminate\Database\Seeder;
use App\Models\Seller;

class SellerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Seller::class, 50)->create();
    }
}
